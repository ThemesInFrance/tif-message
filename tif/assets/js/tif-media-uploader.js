// jQuery(document).ready(function($){
//
// 	var mediaUploader;
//
// 	$('.btn-upload-image').click(function(e) {
// 	formfieldID=jQuery(this).prev().attr("id");
// 	e.preventDefault();
// 	// If the uploader object has already been created, reopen the dialog
// 		if (mediaUploader) {
// 		mediaUploader.open();
// 		return;
// 	}
// 	// Extend the wp.media object
// 	mediaUploader = wp.media.frames.file_frame = wp.media({
// 		title: 'Choose Image',
// 		button: {
// 		text: 'Choose Image'
// 	}, multiple: false });
//
// 	// When a file is selected, grab the URL and set it as the text field's value
// 	mediaUploader.on('select', function() {
// 		var attachment = mediaUploader.state().get('selection').first().toJSON();
// 		jQuery("#"+formfieldID).val(attachment.url);
// 	});
// 	// Open the uploader dialog
// 	mediaUploader.open();
// 	});
//
// });

jQuery(document).ready(function($){

	$('body').delegate('.btn-upload-image', 'click', function(e){
		e.preventDefault();

		var $image = $(this).siblings('.image-preview');
		var $input = $(this).siblings('.image-url');
		var $inputID = $(this).siblings('.image-id');
		var $remove_btn = $(this).siblings('.btn-remove-image');

		var uploader = wp.media({
			title: 'Choose image',
			button: {
				text: 'Select'
			},
			multiple:false
		})
		.on('select', function(){
			var attachment = uploader.state().get('selection').first().toJSON();

			// DEBUG:
			// console.log(attachment);
			// var thumb = attachment.sizes.thumbnail.url;
			// alert(thumb);

			$image.attr('src', attachment.sizes.thumbnail.url).removeClass('hidden');
			$remove_btn.removeClass('hidden');
			// $input.val(attachment.url);
			$input.val(attachment.url);
			$inputID.val(attachment.id);

		}).open();
	});

	$('body').delegate('.btn-remove-image', 'click', function(e){
		var $image = $(this).siblings('.image-preview');
		var $input = $(this).siblings('.image-url');
		var $inputID = $(this).siblings('.image-id');

		$image.addClass('hidden');
		$image.attr('src', '');
		$(this).addClass('hidden');
		$input.val('');
		$inputID.val('');
	});
});
