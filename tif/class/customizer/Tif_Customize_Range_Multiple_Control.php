<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_extend_range_multiple_control' ) ) {

	add_action( 'customize_register', 'tif_extend_range_multiple_control' );

	function tif_extend_range_multiple_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		if ( ! class_exists ( 'Tif_Customize_Range_Multiple_Control' ) ) {

			class Tif_Customize_Range_Multiple_Control extends WP_Customize_Control {

				public $type = 'tif-range-multiple';

				public function render_content() {

					/* if no choices, bail. */
					if ( empty( $this->choices ) )
						return;

					$name = '_customize-' . $this->id;

					if ( ! empty( $this->label ) ) // add label if needed.
						echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

					if ( ! empty( $this->description ) ) // add desc if needed.
						echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

					$values    = !is_array( $this->value() ) ? explode( ',', $this->value() ) : $this->value();
					$alignment = isset ( $this->input_attrs['alignment'] ) ? $this->input_attrs['alignment'] : 'row' ;

					?>

					<ul class="tif-multirange <?php echo esc_attr( $alignment ) ?>">

						<?php

						$i = 0;
						foreach ( $this->choices as $value => $label ) :

						?>

						<li>

								<label>

									<?php

									echo esc_html( $label );

									?>

									<input type="range" value="<?php echo (float)$values[$i]; ?>"

										<?php

										echo ( isset ( $this->input_attrs['min'] ) ? ' min="' . (float)$this->input_attrs['min'] . '"' : null );
										echo ( isset ( $this->input_attrs['max'] ) ? ' max="' . (float)$this->input_attrs['max'] . '"' : null );
										echo ( isset ( $this->input_attrs['step'] ) ? ' step="' . (float)$this->input_attrs['step'] . '"' : null );

										?>

									/>

								</label>

							</li>

						<?php

						++$i;
						endforeach;

						?>

						<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $values ) ); ?>" />

				</ul>

				<?php

				}

			}

		} // end if class_exists

	}

}
