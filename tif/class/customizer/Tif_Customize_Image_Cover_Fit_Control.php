<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
* TODO
*/
if ( ! function_exists( 'tif_customize_image_cover_fit_control' ) ) {

	add_action( 'customize_register', 'tif_customize_image_cover_fit_control' );

	function tif_customize_image_cover_fit_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Image_Cover_Fit_Control extends WP_Customize_Control {

			public $type = 'tif-cover-fit';

			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				$values       = tif_sanitize_cover_fit( $this->value() );
				$descriptions = $this->description;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( isset ( $descriptions['main'] ) && $descriptions['main'] ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $descriptions['main'], tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-cover-fit-control">

					<li class="customize-control-cover-fit-preset">

						<label>
							<?php
							echo '<strong class="tif-customizer-sub-title">' . __( 'Image size', 'canopee' ) . '</strong>';
							if ( isset ( $descriptions['preset'] ) && $descriptions['preset'] )
								echo sprintf( '<span class="tif-customizer-description">%s</span>',
								esc_html( $descriptions['preset'] )
							);
							?>
							<select class="cover-fit-preset">
								<?php

								foreach ( $this->choices as $value => $label ){

									echo '<option value="' . tif_sanitize_key( $value ) . '" ' . selected( $values[0], $value ) . '>' . esc_html( $label ) . '</option>';

								}

								?>
							</select>

						</label>

					</li>

					<li class="customize-control-cover-fit-position">
						<?php
						echo '<strong class="tif-customizer-sub-title">' . __( 'Image position', 'canopee' ) . '</strong>';
						if ( isset ( $descriptions['position'] ) && $descriptions['position'] )
							echo sprintf( '<span class="tif-customizer-description">%s</span>',
							esc_html( $descriptions['position'] )
						);
						?>
						<div class="customize-control-content">
							<fieldset>
								<legend class="screen-reader-text"><span><?php esc_html_e( 'Image position', 'canopee' ) ?></span></legend>
								<div class="cover-fit-position-control">
									<div class="button-group">
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="left top"
											<?php
											checked( $values[1], "left top" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-left-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Top Left', 'canopee' ) ?></span>
										</label>
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="center top"
											<?php
											checked( $values[1], "center top" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-up-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Top', 'canopee' ) ?></span>
										</label>
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="right top"
											<?php
											checked( $values[1], "right top" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-right-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Top Right', 'canopee' ) ?></span>
										</label>
									</div>
									<div class="button-group">
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="left center"
											<?php
											checked( $values[1], "left center" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-left-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Left', 'canopee' ) ?></span>
										</label>
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="center center"
											<?php
											checked( $values[1], "center center" );
											?>>
											<span class="button display-options position"><span class="cover-position-center-icon" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Centre', 'canopee' ) ?></span>
										</label>
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="right center"
											<?php
											checked( $values[1], "right center" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-right-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Right', 'canopee' ) ?></span>
										</label>
									</div>
									<div class="button-group">
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="left bottom"
											<?php
											checked( $values[1], "left bottom" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-left-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Bottom Left', 'canopee' ) ?></span>
										</label>
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="center bottom"
											<?php
											checked( $values[1], "center bottom" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-down-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Bottom', 'canopee' ) ?></span>
										</label>
										<label>
											<input class="ui-helper-hidden-accessible" name="<?php echo tif_sanitize_slug( $name ) ?>" type="radio" value="right bottom"
											<?php
											checked( $values[1], "right bottom" );
											?>>
											<span class="button display-options position"><span class="dashicons dashicons-arrow-right-alt" aria-hidden="true"></span></span>
											<span class="screen-reader-text"><?php esc_html_e( 'Bottom Right', 'canopee' ) ?></span>
										</label>
									</div>
								</div>
							</fieldset>
						</div>
					</li>

					<li class="customize-control-cover-fit-height">
						<?php
						echo '<strong class="tif-customizer-sub-title">' . __( 'Min. height', 'canopee' ) . '</strong>';
						if ( isset ( $descriptions['height'] ) && $descriptions['height'] )
							echo sprintf( '<span class="tif-customizer-description">%s</span>',
							esc_html( $descriptions['height'] )
						);
						?>
						<ul class="tif-multinumber column has-unit">

							<li>

								<label>

									<?php
									echo esc_html__( 'Value', 'canopee' );
									?>

									<input class="cover-fit-preset-height-value" type="number" value="<?php echo $values[2]; ?>"
									<?php
									echo ( isset ( $this->input_attrs['min'] ) ? ' min="' . (float)$this->input_attrs['min'] . '"' : null );
									echo ( isset ( $this->input_attrs['max'] ) ? ' max="' . (float)$this->input_attrs['max'] . '"' : null );
									echo ( isset ( $this->input_attrs['step'] ) ? ' step="' . (float)$this->input_attrs['step'] . '"' : null );
									?>
									/>

								</label>

							<li>

							<li>

								<label>

									<?php
									echo esc_html__( 'Unit', 'canopee' );
									?>

									<select class="cover-fit-preset-height-unit">
										<?php

										foreach ( $this->input_attrs['unit'] as $val=> $option ) :
											echo '<option value="' . esc_html( $val ) . '"' . ( $values[3] == $val ? ' selected' : null ) . '>' . esc_html( $option ) . '</option>';
										endforeach;

										?>

									</select>

								</label>

							</li>

						</ul>

					</li>

					<li class="customize-control-cover-fit-fixed">
						<label>
							<input class="cover-fixed" type="checkbox" value="1"
							<?php
							checked( (bool)$values[4], true );
							?>>
						<?php esc_html_e( 'Image is fixed', 'canopee' ) ?></label>
					</li>

					<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( implode( ',', $values ) ); ?>" />

				</ul>

				<?php

			}

		}

	}

}
