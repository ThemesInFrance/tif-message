<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * TODO
 */
if ( ! function_exists( 'tif_wrap_attr_content_control' ) ) {

	add_action( 'customize_register', 'tif_wrap_attr_content_control' );

	function tif_wrap_attr_content_control( $wp_customize ) {

		if ( ! class_exists( 'WP_Customize_Control' ) )
			return null;

		class Tif_Customize_Wrap_Attr_Content_Control extends WP_Customize_Control {

			public $type = 'tif-wrap-attr';

			public function render_content() {

				/* if no choices, bail. */
				if ( empty( $this->choices ) )
					return;

				$name = '_customize-' . $this->id;

				if ( ! empty( $this->label ) ) // add label if needed.
					echo '<label class="customize-control-title tif-customizer-title">' . esc_html( $this->label ) . '</label>';

				if ( ! empty( $this->description ) ) // add desc if needed.
					echo '<span class="customize-control-description tif-customizer-description">' .  wp_kses( $this->description, tif_allowed_html() ) . '</span>';

				?>

				<ul class="tif-wrap-container-attr-control">

				<?php

				$values       = tif_sanitize_multicheck( $this->value() );
				$hidden_value = is_array( $this->value() ) ? implode( ',', $this->value() ) : $this->value() ;

				foreach ( $this->choices as $key => $value ) {

					if( isset ( $value['id'] ) ) {

						$value = is_array($value) ? $value : array();

						if( $value['id'] == 'content' ) {

							?>

							<li>
								<label>
								<?php

								if ( isset ( $value['label'] ) && $value['label'] ) {
									echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
								} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
									echo '<strong class="tif-customizer-sub-title">' . __( 'Content', 'canopee' ) . '</strong>';
								}

								if ( isset ( $value['description'] ) && $value['description'] )
									echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);

								if ( isset ( $value['type'] ) && $value['type'] == "text" ) {

									echo '<input class="content" type="text" value="' . esc_html( isset ( $values[$key] ) ? (string)$values[$key] : null ) . '" />';

								} else {

									echo '<textarea class="content" style="width:100%;height:100px;">' . esc_html( isset ( $values[$key] ) ? (string)$values[$key] : null ) . '</textarea>';

								}
								?>


							</li>

							<?php

						}

						if( $value['id'] == 'container_tag' ) {

							$selected = isset ( $values[$key] ) ? (string)$values[$key] : 'div';

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Container tag', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);
								?>
								<select class="container-tag">
									<option value="h1" <?php selected( $selected, 'h1' ); ?>><?php _e( 'h1', 'canopee' ) ?></option>
									<option value="h2" <?php selected( $selected, 'h2' ); ?>><?php _e( 'h2', 'canopee' ) ?></option>
									<option value="h3" <?php selected( $selected, 'h3' ); ?>><?php _e( 'h3', 'canopee' ) ?></option>
									<option value="h4" <?php selected( $selected, 'h4' ); ?>><?php _e( 'h4', 'canopee' ) ?></option>
									<option value="h5" <?php selected( $selected, 'h5' ); ?>><?php _e( 'h5', 'canopee' ) ?></option>
									<option value="h6" <?php selected( $selected, 'h6' ); ?>><?php _e( 'h6', 'canopee' ) ?></option>
									<option value="div" <?php selected( $selected, 'div' ); ?>><?php _e( 'div', 'canopee' ) ?></option>
									<option value="div" <?php selected( $selected, 'div' ); ?>><?php _e( 'div', 'canopee' ) ?></option>
									<option value="p" <?php selected( $selected, 'p' ); ?>><?php _e( 'p', 'canopee' ) ?></option>
									<option value="span" <?php selected( $selected, 'span' ); ?>><?php _e( 'span', 'canopee' ) ?></option>
									<option value="strong" <?php selected( $selected, 'strong' ); ?>><?php _e( 'strong', 'canopee' ) ?></option>
									<option value="i" <?php selected( $selected, 'i' ); ?>><?php _e( 'i', 'canopee' ) ?></option>
								</select>
							</label>
						</li>

						<?php

						}

						if( $value['id'] == 'container_class' ) {

							?>

							<li>
								<label>
									<?php

									if ( isset ( $value['label'] ) && $value['label'] ) {
										echo '<strong class="tif-customizer-sub-title">' . esc_html( $value['label'] ) . '</strong>';
									} elseif( ! isset ( $value['label'] ) || $value['label'] != false ) {
										echo '<strong class="tif-customizer-sub-title">' . __( 'Additional CSS Class(es)', 'canopee' ) . '</strong>';
									}

									if ( isset ( $value['description'] ) && $value['description'] )
										echo sprintf( '<span class="tif-customizer-description">%s</span>',
										esc_html( $value['description'] )
									);
									?>
									<input class="container-class" type="text" value="<?php echo esc_attr( ( isset ( $values[$key] ) ? (string)$values[$key] : null ) ); ?>" />
								</label>
							</li>

							<?php

						}

					}

				}

				?>

				<input type="hidden" <?php $this->link(); ?> value="<?php echo esc_attr( $hidden_value ); ?>" />
				</ul>

				<?php
			}

		}

	}

}
