<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * [tif_get_scss_spacers_properties description]
 * @param  boolean $keys                [description]
 * @param  boolean $panel               [description]
 * @return [type]         [description]
 * @TODO
 */
function tif_get_scss_spacers_properties( $keys = false, $panel = false ) {

	$properties = array(

		// Spacers
		'padding' => array( 'panel' => 'spacers',     'label' => esc_html_x( 'Padding', 'Scss utilities', 'canopee' ) ),
		'margin'  => array( 'panel' => 'spacers',     'label' => esc_html_x( 'Margin', 'Scss utilities', 'canopee' ) ),

		// With Breakpoints
		'has_bp'  => array( 'panel' => 'breakpoints', 'label' => esc_html_x( 'Use spacers breakpoints', 'Scss utilities', 'canopee' ) ),
	);

	if ( $keys )
		return array_keys( $properties );

	if ( $panel ) {
		foreach ( $properties as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_scss_global_properties description]
 * @param  boolean $keys                [description]
 * @param  boolean $panel               [description]
 * @return [type]         [description]
 * @TODO
 */
function tif_get_scss_global_properties( $keys = false, $panel = false ) {

	$properties = array(

		// Display Values
		'display'         => array( 'panel' => 'display',     'label' => esc_html_x( 'Display values', 'Scss utilities', 'canopee' ) ),

		// Flexbox properties
		'flex'            => array( 'panel' => 'flex',        'label' => esc_html_x( 'Flexbox properties', 'Scss utilities', 'canopee' ) ),

		// Float properties
		'float'           => array( 'panel' => 'float',       'label' => esc_html_x( 'Float properties', 'Scss utilities, Float properties', 'canopee' ) ),

		// Text properties
		'text_transform'  => array( 'panel' => 'text',        'label' => esc_html_x( 'text-transform', 'Scss utilities', 'canopee' ) ),
		'text_align'      => array( 'panel' => 'text',        'label' => esc_html_x( 'text-align', 'Scss utilities', 'canopee' ) ),
		'font_weight'     => array( 'panel' => 'text',        'label' => esc_html_x( 'font-weight', 'Scss utilities', 'canopee' ) ),
		'font_style'      => array( 'panel' => 'text',        'label' => esc_html_x( 'font-style', 'Scss utilities', 'canopee' ) ),
		'font_size'       => array( 'panel' => 'text',        'label' => esc_html_x( 'font-size', 'Scss utilities', 'canopee' ) ),

		// Alignment properties
		'justify_content' => array( 'panel' => 'alignment',   'label' => esc_html_x( 'justify-content', 'Scss utilities', 'canopee' ) ),
		'justify_items'   => array( 'panel' => 'alignment',   'label' => esc_html_x( 'justify-items', 'Scss utilities', 'canopee' ) ),
		'justify_self'    => array( 'panel' => 'alignment',   'label' => esc_html_x( 'justify-self', 'Scss utilities', 'canopee' ) ),
		'align_content'   => array( 'panel' => 'alignment',   'label' => esc_html_x( 'align-content', 'Scss utilities', 'canopee' ) ),
		'align_items'     => array( 'panel' => 'alignment',   'label' => esc_html_x( 'align-items', 'Scss utilities', 'canopee' ) ),
		'align_self'      => array( 'panel' => 'alignment',   'label' => esc_html_x( 'align-self', 'Scss utilities', 'canopee' ) ),
		'vertical_align'  => array( 'panel' => 'alignment',   'label' => esc_html_x( 'vertical-align', 'Scss utilities', 'canopee' ) ),

		// Placement values
		'placement'       => array( 'panel' => 'placement',   'label' => esc_html_x( 'Placement properties', 'Scss utilities', 'canopee' ) ),

		// With Breakpoints
		'has_bp'          => array( 'panel' => 'breakpoints', 'label' => esc_html_x( 'Use global breakpoints', 'Scss utilities', 'canopee' ) ),
	);

	if ( $keys )
		return array_keys( $properties );

	if ( $panel ) {
		foreach ( $properties as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_scss_grid_properties description]
 * @param  boolean $keys                [description]
 * @param  boolean $panel               [description]
 * @return [type]         [description]
 * @TODO
 */
function tif_get_scss_grid_properties( $keys = false, $panel = false ) {

	$properties = array(

		// Grid properties
		'grid'                  => array( 'panel' => 'grid',      'label' => esc_html_x( 'Grid', 'Scss utilities', 'canopee' ) ),
		'grid_col'              => array( 'panel' => 'grid',      'label' => esc_html_x( 'Grid columns', 'Scss utilities', 'canopee' ) ),
		'grid_has_bp'           => array( 'panel' => 'grid',      'label' => esc_html_x( 'Use grid breakpoints', 'Scss utilities', 'canopee' ) ),

		// Gap properties
		'grid_gap'              => array( 'panel' => 'gap',       'label' => esc_html_x( 'Gap', 'Scss utilities', 'canopee' ) ),
		'grid_col_gap'          => array( 'panel' => 'gap',       'label' => esc_html_x( 'Columns gap', 'Scss utilities', 'canopee' ) ),
		'grid_row_gap'          => array( 'panel' => 'gap',       'label' => esc_html_x( 'Row gap', 'Scss utilities', 'canopee' ) ),
		'grid_gap_has_bp'       => array( 'panel' => 'gap',       'label' => esc_html_x( 'Use grid breakpoints', 'Scss utilities', 'canopee' ) ),

		// Alignment properties
		'grid_col_placement'    => array( 'panel' => 'placement', 'label' => esc_html_x( 'Columns placement', 'Scss utilities', 'canopee' ) ),
		'grid_row_placement'    => array( 'panel' => 'placement', 'label' => esc_html_x( 'Row placement', 'Scss utilities', 'canopee' ) ),
		'grid_placement_has_bp' => array( 'panel' => 'placement', 'label' => esc_html_x( 'Use grid breakpoints', 'Scss utilities', 'canopee' ) ),

		// Span properties
		'grid_col_span'         => array( 'panel' => 'span',      'label' => esc_html_x( 'Columns span', 'Scss utilities', 'canopee' ) ),
		'grid_row_span'         => array( 'panel' => 'span',      'label' => esc_html_x( 'Row span', 'Scss utilities', 'canopee' ) ),
		'grid_span_has_bp'      => array( 'panel' => 'span',      'label' => esc_html_x( 'Use grid breakpoints', 'Scss utilities', 'canopee' ) ),

	);

	if ( $keys )
		return array_keys( $properties );

	if ( $panel ) {
		foreach ( $properties as $key => $value ) {
			if ( array_search ( $panel, $value ) )
				$array[$key] = $value['label'];
		}
	}

	return ( ! empty( $array ) ? $array : $properties );


}

/**
 * [tif_get_custom_length_array description]
 * @param  string $get               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_custom_length_array( $get = 'array' ) {

	$tif_length_values = tif_get_option( 'theme_assets', 'tif_length_values', 'multicheck' );

	$default = array(
		.625,
		1.25,
		1.875,
		'rem'
	);

	$parsed = tif_parse_args_recursive( $tif_length_values, $default );

	$properties = array(

		'tif_length_small'  => array( 'label' => esc_html__( 'Custom length "small"', 'canopee' ),  'value' => (float)$parsed[0] . ',' . (string)$parsed[3] ),
		'tif_length_medium' => array( 'label' => esc_html__( 'Custom length "medium"', 'canopee' ), 'value' => (float)$parsed[1] . ',' . (string)$parsed[3] ),
		'tif_length_large'  => array( 'label' => esc_html__( 'Custom length "large"', 'canopee' ),  'value' => (float)$parsed[2] . ',' . (string)$parsed[3] ),

	);

	if ( $get == 'keys' )
		return array_keys( $properties );

	if ( $get != 'array' && ( $get == 'label' || $get == 'value' ) ) {
		foreach ( $properties as $key => $value ) {
			(string)$array[$key] = $value[(string)$get];
		}
	}
	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_spacers_array description]
 * @param  string $get               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_spacers_array( $get = 'array' ) {

	$properties = array(

		'spacer_none'        => array( 'label' => esc_html__( 'None', 'canopee' ),             'value' => '0' ),             // none
		'spacer_tiny'        => array( 'label' => esc_html__( 'Spacer "Tiny"', 'canopee' ),    'value' => '.125,rem' ),      // 2
		'spacer_tiny_plus'   => array( 'label' => esc_html__( 'Spacer "Tiny+"', 'canopee' ),   'value' => '.313,rem' ),      // 5
		'spacer_small'       => array( 'label' => esc_html__( 'Spacer "Small"', 'canopee' ),   'value' => '.5,rem', ),       // 8
		'spacer_small_plus'  => array( 'label' => esc_html__( 'Spacer "Small+"', 'canopee' ),  'value' => '.625,rem' ),      // 10
		'spacer_medium'      => array( 'label' => esc_html__( 'Spacer "Medium"', 'canopee' ),  'value' => '1,rem' ),         // 16
		'spacer_medium_plus' => array( 'label' => esc_html__( 'Spacer "Medium+"', 'canopee' ), 'value' => '1.25,rem' ),      // 20
		'spacer_large'       => array( 'label' => esc_html__( 'Spacer "Large"', 'canopee' ),   'value' => '1.5,rem' ),       // 24
		'spacer_large_plus'  => array( 'label' => esc_html__( 'Spacer "Large+"', 'canopee' ),  'value' => '2.25,rem' ),      // 36

	);

	if ( $get == 'keys' )
		return array_keys( $properties );

	if( $get == 'label' )
		$properties = array_merge( $properties, tif_get_custom_length_array() );

	if ( $get != 'array' && ( $get == 'label' || $get == 'value' ) ) {
		foreach ( $properties as $key => $value ) {
			(string)$array[$key] = $value[(string)$get];
		}
	}
	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_gap_array description]
 * @param  string $get               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_gap_array( $get = 'array' ) {

	$properties = array(

		'gap_none'   => array( 'label' => esc_html__( 'None', 'canopee' ),         'value' => '0' ),                         // none
		'gap_tiny'   => array( 'label' => esc_html__( 'Gap "Tiny"', 'canopee' ),   'value' => '.313,rem' ),                  // 5
		'gap_small'  => array( 'label' => esc_html__( 'Gap "Small"', 'canopee' ),  'value' => '.625,rem' ),                  // 10
		'gap_medium' => array( 'label' => esc_html__( 'Gap "Medium"', 'canopee' ), 'value' => '1,rem' ),                     // 16
		'gap_large'  => array( 'label' => esc_html__( 'Gap "Large"', 'canopee' ),  'value' => '1.25,rem' ),                  // 20

	);

	if ( $get == 'keys' )
		return array_keys( $properties );

	if( $get == 'label' )
		$properties = array_merge( $properties, tif_get_custom_length_array() );

	if ( $get != 'array' && ( $get == 'label' || $get == 'value' ) ) {
		foreach ( $properties as $key => $value ) {
			(string)$array[$key] = $value[(string)$get];
		}
	}
	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_theme_colors_array description]
 * @param  string $get               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_theme_colors_array( $get = 'array' ) {

	$palette    = tif_get_option( 'theme_colors', 'tif_palette_colors', 'array' );
	$semantic   = tif_get_option( 'theme_colors', 'tif_semantic_colors', 'array' );

	$properties = array(
		'light'        => array( 'label' => esc_html_x( 'Light shades', 'Theme colors', 'canopee' ),           'value' => $palette['light'] ),
		'light_accent' => array( 'label' => esc_html_x( 'Light accent', 'Theme colors', 'canopee' ),           'value' => $palette['light_accent'] ),
		'primary'      => array( 'label' => esc_html_x( 'Main color', 'Theme colors', 'canopee' ),             'value' => $palette['primary'] ),
		'dark_accent'  => array( 'label' => esc_html_x( 'Dark accent', 'Theme colors', 'canopee' ),            'value' => $palette['dark_accent'] ),
		'dark'         => array( 'label' => esc_html_x( 'Dark shades', 'Theme colors', 'canopee' ),            'value' => $palette['dark'] ),
		'default'      => array( 'label' => esc_html_x( 'Default semantic color', 'Theme colors', 'canopee' ), 'value' => $semantic['default'] ),
		'info'         => array( 'label' => esc_html_x( 'Info color', 'Theme colors', 'canopee' ),             'value' => $semantic['info'] ),
		'success'      => array( 'label' => esc_html_x( 'Success color', 'Theme colors', 'canopee' ),          'value' => $semantic['success'] ),
		'warning'      => array( 'label' => esc_html_x( 'Warning color', 'Theme colors', 'canopee' ),          'value' => $semantic['warning'] ),
		'danger'       => array( 'label' => esc_html_x( 'Danger color', 'Theme colors', 'canopee' ),           'value' => $semantic['danger'] ),
		'white'        => array( 'label' => esc_html_x( 'White', 'Theme colors', 'canopee' ),                  'value' => '#ffffff' ),
		'black'        => array( 'label' => esc_html_x( 'Black', 'Theme colors', 'canopee' ),                  'value' => '#000000' ),
		'none'         => array( 'label' => esc_html_x( 'None', 'Theme colors', 'canopee' ),                   'value' => false ),
	);

	if ( $get == 'keys' )
		return array_keys( $properties );

	if ( $get != 'array' && ( $get == 'label' || $get == 'value' ) ) {
		foreach ( $properties as $key => $value ) {
			(string)$array[$key] = $value[(string)$get];
		}
	}
	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_border_radius_array description]
 * @param  string $get               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_border_radius_array( $get = 'array' ) {

	$properties = array(

		'radius_none'   => array( 'label' => esc_html__( 'None', 'canopee' ),   'value' => '0' ),                            // none
		'radius_tiny'   => array( 'label' => esc_html__( 'Tiny', 'canopee' ),   'value' => '.125,rem' ),                     // 2
		'radius_small'  => array( 'label' => esc_html__( 'Small', 'canopee' ),  'value' => '.5,rem' ),                       // 8
		'radius_medium' => array( 'label' => esc_html__( 'Medium', 'canopee' ), 'value' => '1,rem' ),                        // 16
		'radius_large'  => array( 'label' => esc_html__( 'Large', 'canopee' ),  'value' => '2,rem' ),                        // 32
		'radius_circle' => array( 'label' => esc_html__( 'Circle', 'canopee' ), 'value' => '50,%' ),                         // circle

	);

	if ( $get == 'keys' )
		return array_keys( $properties );

	if ( $get != 'array' && ( $get == 'label' || $get == 'value' ) ) {
		foreach ( $properties as $key => $value ) {
			(string)$array[$key] = $value[(string)$get];
		}
	}
	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_border_width_array description]
 * @param  string $get               [description]
 * @return [type]      [description]
 * @TODO
 */
function tif_get_border_width_array( $get = 'array' ) {

	$properties = array(

		'width_none'   => array( 'label' => esc_html__( 'None', 'canopee' ),        'value' => '0' ),
		'width_tiny'   => array( 'label' => esc_html__( 'Tiny', 'canopee' ),        'value' => '1,px' ),
		'width_small'  => array( 'label' => esc_html__( 'Small', 'canopee' ),       'value' => '2,px' ),
		'width_medium' => array( 'label' => esc_html__( 'Medium', 'canopee' ),      'value' => '3,px' ),
		'width_large'  => array( 'label' => esc_html__( 'Large', 'canopee' ),       'value' => '5,px' ),
		'width_xlarge' => array( 'label' => esc_html__( 'Extra Large', 'canopee' ), 'value' => '10,px' ),

	);

	if ( $get == 'keys' )
		return array_keys( $properties );

	if ( $get != 'array' && ( $get == 'label' || $get == 'value' ) ) {
		foreach ( $properties as $key => $value ) {
			(string)$array[$key] = $value[(string)$get];
		}
	}
	return ( ! empty( $array ) ? $array : $properties );

}

/**
 * [tif_get_flex_direction_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_flex_direction_options() {

	return array(

		'row'            => esc_html__( '"row"', 'canopee' ),
		'row_reverse'    => esc_html__( '"row-reverse"', 'canopee' ),
		'column'         => esc_html__( '"column"', 'canopee' ),
		'column_reverse' => esc_html__( '"column-reverse"', 'canopee' ),

	);

}

/**
 * [tif_get_flex_wrap_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_flex_wrap_options() {

	return array(

		''             => esc_html__( 'Undefined', 'canopee' ),
		'wrap'         => esc_html__( '"wrap"', 'canopee' ),
		'nowrap'       => esc_html__( '"nowrap"', 'canopee' ),
		'wrap_reverse' => esc_html__( '"wrap-reverse"', 'canopee' ),

	);

}

/**
 * [tif_get_justify_content_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_justify_content_options() {

	return array(

		''              => esc_html__( 'Undefined', 'canopee' ),
		'start'         => esc_html__( '"start"', 'canopee' ),
		'center'        => esc_html__( '"center"', 'canopee' ),
		'end'           => esc_html__( '"end"', 'canopee' ),
		'space_evenly'  => esc_html__( '"space-evenly"', 'canopee' ),
		'space_around'  => esc_html__( '"space-around"', 'canopee' ),
		'space_between' => esc_html__( '"space-between"', 'canopee' ),

	);

}

/**
 * [tif_get_align_items_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_align_items_options() {

	return array(

		''              => esc_html__( 'Undefined', 'canopee' ),
		'start'         => esc_html__( '"start"', 'canopee' ),
		'center'        => esc_html__( '"center"', 'canopee' ),
		'end'           => esc_html__( '"end"', 'canopee' ),
		'strech'        => esc_html__( '"stretch"', 'canopee' ),
		'space_around'  => esc_html__( '"space-around"', 'canopee' ),
		'space_between' => esc_html__( '"space-between"', 'canopee' ),

	);

}

/**
 * [tif_get_align_content_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_align_content_options() {

	return array(

		''              => esc_html__( 'Undefined', 'canopee' ),
		'start'         => esc_html__( '"start"', 'canopee' ),
		'center'        => esc_html__( '"center"', 'canopee' ),
		'end'           => esc_html__( '"end"', 'canopee' ),
		'strech'        => esc_html__( '"stretch"', 'canopee' ),
		'space_around'  => esc_html__( '"space-around"', 'canopee' ),
		'space_between' => esc_html__( '"space-between"', 'canopee' ),

	);

}

/**
 * [tif_get_font_size_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_font_size_options() {

	return array(

		''                      => esc_html_x( 'Default', 'Font size', 'canopee' ),
		'font_size_normal'      => esc_html_x( 'Normal', 'Font size', 'canopee' ),
		'font_size_tiny'        => esc_html_x( 'Extra Small', 'Font size', 'canopee' ),
		'font_size_small'       => esc_html_x( 'Small', 'Font size', 'canopee' ),
		'font_size_medium'      => esc_html_x( 'Medium', 'Font size', 'canopee' ),
		'font_size_large'       => esc_html_x( 'Large', 'Font size', 'canopee' ),
		'font_size_extra_large' => esc_html_x( 'Extra Large', 'Font size', 'canopee' ),
		'font_size_huge'        => esc_html_x( 'Huge', 'Font size', 'canopee' ),
		'inherit'               => esc_html_x( 'Inherit (inherit)', 'Font size', 'canopee' ),
		'initial'               => esc_html_x( 'Initial (initial)', 'Font size', 'canopee' ),
		// 'small'                 => esc_html_x( 'Small (small)', 'Font size', 'canopee' ),
		// 'smaller'               => esc_html_x( 'Smaller (smaller)', 'Font size', 'canopee' ),
		// 'large'                 => esc_html_x( 'Large (large)', 'Font size', 'canopee' ),
		// 'larger'                => esc_html_x( 'Larger (larger)', 'Font size', 'canopee' ),

	);

}

/**
 * [tif_get_font_stack_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_font_stack_options() {

	return array(

		''                => esc_html_x( 'Default', 'Font size', 'canopee' ),
		'system_based'    => esc_html_x( 'System fonts', 'Font stack', 'canopee' ),
		'times_based'     => esc_html_x( 'Times New Roman based', 'Font stack', 'canopee' ),
		'georgia_based'   => esc_html_x( 'Georgia based', 'Font stack', 'canopee' ),
		'garamond_based'  => esc_html_x( 'Garamond based', 'Font stack', 'canopee' ),
		'helvetica_based' => esc_html_x( 'Helvetica based', 'Font stack', 'canopee' ),
		'verdana_based'   => esc_html_x( 'Verdana based', 'Font stack', 'canopee' ),
		'trebuchet_based' => esc_html_x( 'Trebuchet based', 'Font stack', 'canopee' ),
		'impact_based'    => esc_html_x( 'Impact based', 'Font stack', 'canopee' ),
		'monospace_based' => esc_html_x( 'Monospace based', 'Font stack', 'canopee' ),

	);

}

/**
 * [tif_get_icon_size_options description]
 * @return [type] [description]
 * @TODO
 */
function tif_get_icon_size_options() {

	return array(

		'tiny'   => esc_html__( 'Tiny', 'canopee' ),
		'small'  => esc_html__( 'Small', 'canopee' ),
		'medium' => esc_html__( 'Medium', 'canopee' ),
		'large'  => esc_html__( 'Large', 'canopee' ),
		'xlarge' => esc_html__( 'Extra Large', 'canopee' ),

	);

}
