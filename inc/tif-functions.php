<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Notification Bar front render function
 */

add_action( 'init', 'tif_notification_bar_setcookie');
function tif_notification_bar_setcookie() {

	global $notification_bar_init;
	$notification_bar_init	 = tif_get_option( 'plugin_notification_bar', 'tif_init', 'array' );

	if ( isset( $_GET['tif-notification-bar-id'] ) && (int)$_GET['tif-notification-bar-id'] == (int)$notification_bar_init['id'] )
		setcookie( "Tif_Notification_Bar_". (int)$notification_bar_init['id'], "true", time() + (86400 * 2), "/");

}

function tif_render_notification_bar() {

	global $notification_bar_init;

	if( isset( $_COOKIE['Tif_Notification_Bar_' . (int)$notification_bar_init['id']] ) && $_COOKIE['Tif_Notification_Bar_' . (int)$notification_bar_init['id']] == 'true' )
		return;

	if ( isset( $_GET['tif-notification-bar-id'] ) && (int)$_GET['tif-notification-bar-id'] == (int)$notification_bar_init['id'] )
		return;

	$today = date("Y-m-d");

	if( null == $notification_bar_init['enabled'] || $today > tif_sanitize_date( $notification_bar_init['date_end'] ) )
		return;

	$notification_bar_layout  = tif_get_option( 'plugin_notification_bar', 'tif_layout', 'array');
	$message		 = explode("\n", str_replace("\r", "", tif_get_option( 'plugin_notification_bar', 'tif_init,message', 'html') ) );

	$btn_txt = $notification_bar_init['close_btn_txt'];
	$btn_tag = 'button';
	$btn_css = null;

	switch ( $notification_bar_init['close_btn'] ) {

		case 'cross':
			$btn_tag = 'span';
			$btn_txt = '&times;';
			$btn_css = 'is-cross';
		break;

		case 'text':
			$btn_tag = 'span';
			$btn_css = 'is-text';
		break;

		case 'button':
			$btn_css = 'is-btn';
		break;

	}

	$button = null != $notification_bar_init['close_btn']
		? '<' .  esc_attr( $btn_tag ) . ' id="tif-notification-bar-btn" onclick="tifSetCookie(\'Tif_Notification_Bar_' .  (int)$notification_bar_init['id'] . '\', true, 2, \'#tif-notification-bar\');" class="js tif-close-btn ' . esc_attr( $btn_css ) . '">' . ( null != $btn_txt ? esc_html__( $btn_txt ) : 'Ok' ) . '</' .  esc_attr( $btn_tag ) . '>'
		: null ;


	$notification_bar_date_start = tif_sanitize_date( $notification_bar_init['date_start'] );
	$notification_bar_date_end   = tif_sanitize_date( $notification_bar_init['date_end'] );
	$today				= tif_sanitize_date( date("Y-m-d") );

	$noscript_class		= ( $today < $notification_bar_date_start ) || ( $today > $notification_bar_date_end ) ? 'hidden ' : null ;

?>

<script>
tifMessage(
	new Date('<?php echo $notification_bar_date_start ?>'),
	new Date('<?php echo $notification_bar_date_end ?>'),
	[
		<?php
		foreach ( $message as $key) {
			echo "'" . addslashes($key) . "',";
		}
		?>
	],
	<?php
		echo "'" . addslashes( $button ) . "'";
	?>
);
</script>

<noscript>
<div id="tif-notification-bar" class="tif-notification-bar <?php echo $noscript_class . ( isset( $notification_bar_layout['layout'] ) && $notification_bar_layout['layout'] ? tif_sanitize_slug( $notification_bar_layout['layout'] ) : 'fixed-bottom' ) ?>">
	<?php

	$current_url  = tif_get_current_url( 'raw' );

	if ( strpos( $current_url, 'tif-notification-bar-id') === false ) {
		$current_url .= strpos( $current_url, '?') !== false ? '&' : '?' ;
		$current_url .= 'tif-notification-bar-id=' . (int)$notification_bar_init['id'];
	}

	$link		  = $notification_bar_init['close_btn'] ? null : '<a href="' . esc_url( $current_url ) . '">';
	?>

	<div class="inner <?php echo ( isset( $notification_bar_layout['boxed'] ) && $notification_bar_layout['boxed'] ? 'tif-boxed': null ) ?>">

		<div>
			<?php
			foreach ( $message as $key) {
				echo "<p>$key</p>";
			}
			?>
		</div>

		<?php

		if ( null != $notification_bar_init['close_btn'] ) {

			echo '<span id="tif-notification-bar-btn" class="tif-close-btn ' . esc_attr( $btn_css ) . '">';
			echo '<a href="' . esc_url( $current_url ) . '">';

			echo ( null != $btn_txt ? esc_html__( $btn_txt ) : 'Ok' );

			echo '</a>';
			echo '</span>';

		}

		?>

	</div>

</div><!-- .tif-notification-bar -->
</noscript>

<?php

}
