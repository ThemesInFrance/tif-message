<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( esc_html__( 'Position', 'tif-notification-bar' ),
	array(
		'type'			=> 'select',
		'selected'		=> tif_get_option( 'plugin_notification_bar', 'tif_layout,layout', 'select' ),
		'options'		=> array(
			'fixed_bottom'	=> esc_html__( 'Bottom', 'tif-notification-bar' ),
			'fixed_top'		=> esc_html__( 'Top', 'tif-notification-bar' ),
			'modal'			=> esc_html__( 'Modal', 'tif-notification-bar' ),
		),
	),
	$tif_plugin_name . '[tif_layout][layout]'
);

$form->add_input( esc_html__( 'Is my message boxed?', 'tif-notification-bar' ),
	array(
		'type'			=> 'checkbox',
		'value'		   	=> 1,
		'checked'		=> tif_get_option( 'plugin_notification_bar', 'tif_layout,boxed', 'checkbox' ),
	),
	$tif_plugin_name . '[tif_layout][boxed]'
);

// if ( ! class_exists ( 'Themes_In_France' ) ) {
	$form->add_input( esc_html__( 'Boxed max width', 'tif-notification-bar' ),
		array(
			'type'		=> 'number',
			'min'		=> '10',
			'max'		=> '3000',
			'step'		=> '1',
			'value'		=> tif_get_option( 'plugin_notification_bar', 'tif_layout,boxed_width', 'int' ),
		),
		$tif_plugin_name . '[tif_layout][boxed_width]'
	);
// }

$form->add_input( esc_html__( 'Background color', 'tif-notification-bar' ),
	array(
		'type'			=> 'text',
		'colorpicker'	=> true,
		'value'			=> tif_get_option( 'plugin_notification_bar', 'tif_layout,bgcolor', 'hexcolor' ),
		'default'		=> tif_get_default( 'plugin_notification_bar', 'tif_layout,bgcolor', 'hexcolor' ),
	),
	$tif_plugin_name . '[tif_layout][bgcolor]'
);

$form->add_input( esc_html__( 'Text Color', 'tif-notification-bar' ),
	array(
		'type'			=> 'text',
		'colorpicker'	=> true,
		'value'			=> tif_get_option( 'plugin_notification_bar', 'tif_layout,text_color', 'hexcolor' ),
		'default'		=> tif_get_default( 'plugin_notification_bar', 'tif_layout,text_color', 'hexcolor' ),
	),
	$tif_plugin_name . '[tif_layout][text_color]'
);

$form->add_input( esc_html__( 'Button background color', 'tif-notification-bar' ),
	array(
		'type'			=> 'text',
		'colorpicker'	=> true,
		'value'			=> tif_get_option( 'plugin_notification_bar', 'tif_layout,close_btn_bgcolor', 'hexcolor' ),
		'default'		=> tif_get_default( 'plugin_notification_bar', 'tif_layout,close_btn_bgcolor', 'hexcolor' ),
	),
	$tif_plugin_name . '[tif_layout][close_btn_bgcolor]'
);

$form->add_input( esc_html__( 'Button text color', 'tif-notification-bar' ),
	array(
		'type'			=> 'text',
		'colorpicker'	=> true,
		'value'			=> tif_get_option( 'plugin_notification_bar', 'tif_layout,close_btn_txt_color', 'hexcolor' ),
		'default'		=> tif_get_default( 'plugin_notification_bar', 'tif_layout,close_btn_txt_color', 'hexcolor' ),
	),
	$tif_plugin_name . '[tif_layout][close_btn_txt_color]'
);
