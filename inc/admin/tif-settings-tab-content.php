<?php

if ( ! defined( 'ABSPATH' ) ) exit;

$form->add_input( esc_html__( 'Message', 'tif-notification-bar' ),
	array(
		'type'            => 'textarea',
		'required'        => true,
		'value'           => tif_get_option( 'plugin_notification_bar', 'tif_init,message', 'html' ),
	),
 	$tif_plugin_name .'[tif_init][message]'
);

$form->add_input( esc_html__( 'Closing button', 'tif-notification-bar' ),
	array(
		'type'            => 'radio',
		'required'        => true,
		'checked'         => tif_get_option( 'plugin_notification_bar', 'tif_init,close_btn', 'radio' ),
		'options'		=> array(
			''				=> esc_html__( 'Unclosable message', 'tif-my-plugin' ),
			'cross'			=> esc_html__( 'Cross button', 'tif-my-plugin' ),
			'text'			=> esc_html__( 'Text only', 'tif-my-plugin' ),
			'button'		=> esc_html__( 'Button', 'tif-my-plugin' ),
		),
	),
	$tif_plugin_name .'[tif_init][close_btn]'
);

$form->add_input( esc_html__( 'Text of the closing button', 'tif-notification-bar' ),
	array(
		'type'            => 'text',
		'value'           => tif_get_option( 'plugin_notification_bar', 'tif_init,close_btn_txt', 'text' ),
	),
	$tif_plugin_name .'[tif_init][close_btn_txt]'
);

$form->add_input( esc_html__( 'Display start date', 'tif-notification-bar' ),
	array(
		'type'            => 'date',
		'required'        => true,
		'value'           => tif_get_option( 'plugin_notification_bar', 'tif_init,date_start', 'date' ),
		'description'     => esc_html__( 'Determines the date from which the message will be displayed.', 'tif-notification-bar' ),
	),
	$tif_plugin_name .'[tif_init][date_start]'
);

$form->add_input( esc_html__( 'Display end date', 'tif-notification-bar' ),
	array(
		'type'            => 'date',
		'required'        => true,
		'value'           => tif_get_option( 'plugin_notification_bar', 'tif_init,date_end', 'date' ),
		'description'     => esc_html__( 'Determines the date after which the message will no longer be displayed. It will remain displayed all day above.', 'tif-notification-bar' ),
	),
	$tif_plugin_name .'[tif_init][date_end]'
);
