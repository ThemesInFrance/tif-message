<?php

if ( ! defined( 'ABSPATH' ) ) exit;

/**
 * Notification Bar Sanitize and register
 */

function tif_notification_bar_sanitize( $input ) {

	// Verify that the nonce is valid.
	if ( ! isset( $_POST['tif_notification_bar_nonce_field'] ) || ! wp_verify_nonce( $_POST['tif_notification_bar_nonce_field'], 'tif_notification_bar_action' ) ) :

		$code	 = 'nonce_unverified';
		$message = esc_html__( 'Sorry, your nonce did not verify. No data was recorded.', 'tif-notification-bar' );
		$type	 = 'error';

		return $input;

	endif;

	$code	 = 'settings_updated';
	$message = esc_html__( 'Congratulations, the data has been successfully recorded.' , 'tif-notification-bar' );
	$type	 = 'updated';

	/**
	 * @link https://developer.wordpress.org/reference/functions/add_settings_error/
	 */
	add_settings_error(
		esc_attr( 'message_settings_error' ),
		esc_attr( $code ),
		esc_html__( $message ),
		$type
	);

	$capability = tif_get_submenu_capability( tif_get_option( 'plugin_notification_bar', 'tif_init,capabilities', 'multicheck' ) ) ;

	if ( ! current_user_can( $capability ) )
		wp_die( esc_html__( 'Unauthorized user.', 'tif-notification-bar' ) );

	$new_input =
		! empty( get_option( 'tif_plugin_notification_bar' ) )
		? get_option( 'tif_plugin_notification_bar' )
		: tif_plugin_notification_bar_setup_data() ;

	// Last Tab used
	$new_input['tif_tabs'] =
		! empty( $input['tif_tabs'] )
		? tif_sanitize_html( $input['tif_tabs'] )
		: false ;

	// Settings
	if(  current_user_can( 'manage_options' ) ) :
		$tif_init = array(
			'enabled'				=> 'checkbox',
			'customizer_enabled'	=> 'checkbox',
			'generated'				=> 'multicheck',
			'css_enabled'			=> 'key',
			'capabilities'			=> 'multicheck',
			'custom_css'			=> 'css',
			'id'					=> 'absint',

			'message'				=> 'html',
			'close_btn'				=> 'key',
			'close_btn_txt'			=> 'string',
			'date_start'			=> 'date',
			'date_end'				=> 'date',
		);

		$input['tif_init']['id'] = time();

		foreach ( $tif_init as $key => $value) {
			$new_input['tif_init'][$key] =
				! empty( $input['tif_init'][$key] )
				? call_user_func( 'tif_sanitize_' . $value, $input['tif_init'][$key] )
				: false ;
		}
	endif;

	// Layout
	$tif_layout = array(
		'layout'				=> 'key',
		'boxed'					=> 'checkbox',
		'boxed_width'			=> 'int',
		'bgcolor'				=> 'hexcolor',
		'text_color'			=> 'hexcolor',
		'close_btn_bgcolor'	=> 'hexcolor',
		'close_btn_txt_color'	=> 'hexcolor',
	);

	foreach ( $tif_layout as $key => $value) {
		$new_input['tif_layout'][$key] =
			! empty( $input['tif_layout'][$key] )
			? call_user_func( 'tif_sanitize_' . $value, $input['tif_layout'][$key] )
			: false ;
	}

	return $new_input;

}
